'''
Documentation, License etc.

@package tweet-sucker
'''
import time
import threading
import logging
import psycopg2
from Queue import Queue
from twython import Twython
from twython import TwythonError, TwythonRateLimitError, TwythonAuthError
from sets import Set
from dateutil import parser
from datetime import datetime

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(filename='twitter.log', level=logging.INFO, format=FORMAT)

APP_KEY = 'RNfXS4TD1iHN8mDKRSMi2g'
APP_SECRET = 'PR4oATkHgj8sQH7OIY6OSDREHqesiX3rupvD0TDXQFg'
ACCESS_TOKEN = 'AAAAAAAAAAAAAAAAAAAAACncFgAAAAAAyb98k7VaZXupIfcYfQLNC7mqx58%3DS9j9k9RQTPgDwab7BWw1dbF83ReNM1uO6gLlBIbuaLk'

'''
Users:
caiorcasimiro
OAUTH_TOKEN = '58598337-3Ut3XyQipUvNP8elbiaPc72b2zgZLjOuTkAsQ0XIV'
OAUTH_TOKEN_SECRET = 'q6ChEOjAaxTqUYYfAaln5CHSw50iy4JWOuo9ehv16xk'

araravermelha2
OAUTH_TOKEN = '1626498452-2sbH8IQzxQTrKQTRFvQlBs6fKOQa3lCZzYIwoBT'
OAUTH_TOKEN_SECRET = '3wjjoISE93WPtlKj7U5HHAf13W8cWeDnHSlTc703HSw'

smart_parrot
OAUTH_TOKEN = '1621675346-b6589IcnejpQs6eMPAyAbTzFkyjCdmL3C8rxWmC'
OAUTH_TOKEN_SECRET = 'Lp0HLxwT2vHPBucoliHiOo2Oa8MkCmYqVGpVvUknOQ'

pardalmarrom
OAUTH_TOKEN = '1626527455-rpjZlooP3yxAsrdoDVgT7Tc3zdz3AAEWRB50EPi'
OAUTH_TOKEN_SECRET = 'IUtsKHcjWmCtDvH2OHl4sAM9QO8qES7imIeFEX7WJ1c'

bemtevi6
OAUTH_TOKEN = '1626520256-Nall1hhzDXPvylDIIXMgftL6xNskNVeRcI1RoZH'
OAUTH_TOKEN_SECRET = '86Y0YWL19xbowTucfcmp0XOlEtVA4AZKUibx1KQiMY'

passaroazul15
OAUTH_TOKEN = '1646396670-qAChtM3tOjoapkCXPIE22bvfECWFBOeoP8Y5FYi'
OAUTH_TOKEN_SECRET = 'UoYqJ46dWZlfDzey4X7KcMA3oW59H4b3DSTB45xqIM'

joaodebarro20
OAUTH_TOKEN = '1646469529-DddaRLJ2ByUtdIEiQTTYNX1U4dfyLWOhK3Zlw9H'
OAUTH_TOKEN_SECRET = 'VxotU2S7KZKqZQkaLcIPZcmzHlnf0rW3ZM7ghvYFQ'

passaromarrom15
OAUTH_TOKEN = '1647550861-c9GBfwHIRfQdM44izOJHIUB5gmc47U4AcSHLGu8'
OAUTH_TOKEN_SECRET = 'ninQXIB85bAIZlqPwYvCykEABRqF3ANXBfUP3ZvQ'

camilamcasimiro
OAUTH_TOKEN = '199298390-sA1VnCRvgvcUSDLv1ScM0BbV9ndxtwjmFc7Rzvtq'
OAUTH_TOKEN_SECRET = 's2MTF9scweFUwMymSl44mviMKWpKG0L2cKCaQs9WgkY'
'''

def retrieve_users(twitter, con):
    estadao_ids = []
    estadao_cursor = -1
    print "Collecting Estadao followers"
    while True:
        try:
            data = twitter.get_followers_ids(screen_name='g1', count=5000, cursor=estadao_cursor)
        except TwythonRateLimitError:
            break
        except Exception, e:
            print "Error:",e
        estadao_ids += data['ids']
        estadao_cursor = data['next_cursor']

    print "Collecting users data"
    cursor = 0
    users = []
    while True:
        if cursor*100 > len(estadao_ids):
            break
        ids = ', '.join(str(x) for x in estadao_ids[(cursor*100):((cursor+1)*100)])
        try:
            r = twitter.lookup_user(user_id=ids)
            users += r
            cursor += 1
        except TwythonRateLimitError:
            break
        except TwythonError, e:
            print e.msg
            continue
        except Exception, e:
            print "Error:",e

    created_after = parser.parse('2011-01-01 00:00:00 +0000')
    created_before = parser.parse('2013-01-01 00:00:00 +0000')
    tweeted_after = parser.parse('2013-06-22 00:00:00 +0000')
    choosen = []
    for u in users:
        if not u.has_key('status'):
            continue
        created_at = parser.parse(u['created_at'])
        tweet_created_at = parser.parse(u['status']['created_at'])

        if u['friends_count'] < 300 and u['statuses_count'] > 1000 and u['statuses_count'] < 5000 and u['protected'] == False and created_at >= created_after and created_at <= created_before and tweet_created_at > tweeted_after:
            choosen.append(u)

    sql = "INSERT INTO twitter_user (id, screen_name, tweets_count, protected, last_tweet_id, last_tweet_time, last_update, created_at) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)"
    now = datetime.now()

    for u in choosen:
        try:
            cur.execute(sql, (u['id'],u['screen_name'],u['statuses_count'],u['protected'],u['status']['id'],parser.parse(u['status']['created_at']),now,parser.parse(u['created_at'])))
            con.commit()
        except Exception, e:
            print "Error on saving", e
            con.rollback()


class RequestTimeline(object):

    def __init__(self, user_id, con, use_oldest_tweet_id = False):
        self.user_id = user_id
        self.con = con
        self.cursor = con.cursor()
        if use_oldest_tweet_id:
            self.oldest = self.get_oldest_tweet_id()
        else:
            self.oldest = None

        self.bulk_tweets = Set()
        self.bulk_mentions = Set()
        self.bulk_urls = Set()

    def add_tweet_to_bulk(self, tweet):
        tweet_time = parser.parse(tweet['created_at'])
        retweeted = None
        if 'retweeted_status' in tweet:
            retweeted = tweet['retweeted_status']['id']

        self.bulk_tweets.add((tweet['id_str'], tweet['text'], tweet_time, self.user_id, retweeted, tweet['retweet_count']))

        for url in tweet['entities']['urls']:
            self.bulk_urls.add((tweet['id_str'], url['expanded_url']))

        for mention in tweet['entities']['user_mentions']:
            self.bulk_mentions.add((self.user_id, mention['id'], tweet['id_str']))

    def persist_bulk(self):
        self.cursor.executemany(
            "INSERT INTO tweet (id, content, creation_time, user_id, retweeted, retweet_count) VALUES (%s, %s, %s, %s, %s, %s)",
            self.bulk_tweets
        )

        self.cursor.executemany(
            "INSERT INTO tweet_url (tweet_id, url) VALUES (%s, %s)",
            self.bulk_urls
        )

        self.cursor.executemany(
            "INSERT INTO mention (user_id, mentioned_id, tweet_id) VALUES (%s, %s, %s)",
            self.bulk_mentions
        )
        logging.info("Persisted bulk load")


    def get_oldest_tweet_id(self):
        cursor = self.con.cursor()
        cursor.execute(
            "SELECT id FROM tweet WHERE user_id = %s ORDER BY id ASC LIMIT 10",
            (self.user_id,)
        )
        result = cursor.fetchone()
        if result is None:
            return result
        else:
            return result[0]


    def do(self, twitter):
        logging.info('Requesting tweets of %s' % self.user_id)

        while True:
            try:
                if self.oldest is not None:
                    data = twitter.get_user_timeline(user_id = self.user_id, count=200, max_id = (self.oldest-1))
                else:
                    data = twitter.get_user_timeline(user_id = self.user_id, count=200)

                if len(data) == 0:
                    break
                for tweet in data:
                    self.add_tweet_to_bulk(tweet)
            except TwythonRateLimitError:
                logging.info("Going to sleep")
                time.sleep((60*15)+30)
                continue
            except TwythonAuthError:
                logging.info("Protected user %d" % self.user_id)
                self.cursor.execute("UPDATE twitter_user SET protected=true WHERE id=%s", (self.user_id,))
                break
            except TwythonError, e:
                if e.error_code == 404:
                    logging.error("Twitter's internal problem. Let the user %s to be collected later"%self.user_id)
                    return
                logging.error(e.msg +  " " + str(self.user_id))
                time.sleep(3)
                continue
            except Exception, e:
                print "Exception RequestTimeline of %s"%self.user_id, e
                time.sleep(3)
                continue

            self.oldest = data[len(data)-1]['id']
        self.persist_bulk()
        self.con.commit()
        
class RequestRelationshipJob(object):
    """Job responsible for retrieving
    followees info from twitter

    """
    FOLLOWERS = 1
    FOLLOWEDS = 2

    def __init__(self, user_id, con, relationship_type):
        self.user_id = user_id
        self.relationship_type = relationship_type
        self.con = con
        self.cursor = con.cursor()
        
    def save_relationship(self, follower, followed):
        self.cursor.execute(
            "INSERT INTO relationship (follower_id, followed_id) VALUES (%s, %s)",
            (follower, followed)
        )

    def do(self, twitter):
        logging.info('Requesting relationships of %s' % self.user_id)

        cursor = -1
        while cursor:
            try:
                if self.relationship_type == RequestRelationshipJob.FOLLOWEDS:
                    data = twitter.get_friends_ids(user_id=self.user_id, cursor=cursor)
                elif self.relationship_type == RequestRelationshipJob.FOLLOWERS:
                    data = twitter.get_followers_ids(user_id=self.user_id, cursor=cursor)
                    
                if data is None:
                    break
                
                for id in data['ids']:
                    if self.relationship_type == RequestRelationshipJob.FOLLOWEDS:
                        self.save_relationship(self.user_id, id)
                    elif self.relationship_type == RequestRelationshipJob.FOLLOWERS:
                        self.save_relationship(id, self.user_id)
                cursor = data['next_cursor']
            except TwythonRateLimitError:
                time.sleep((15*60)+10)
            except TwythonAuthError:
                logging.warn("Protected user: %s ?"%self.user_id)
                return
            except TwythonError, e:
                if e.error_code == 404:
                    logging.error("Twitter's internal problem. Let the user %s to be collected later"%self.user_id)
                    return
                logging.error(e.msg +  " " + str(self.user_id))
                time.sleep(1)
                continue
            except Exception, e:
                print "Exception RequestTimeline of %s"%self.user_id, e
                time.sleep(2)
                continue
        self.con.commit()


class RequestUsersJob(object):
    """Job responsible for requesting user
    info from twitter

    """

    def __init__(self, users, con):
        """Constructor for RequestUserInfoJob"""
        logging.info('Creating RequestUserJob.')
        self.ids = ', '.join(str(x) for x in users)
        self.con = con
        self.cursor = con.cursor()
        self.now = datetime.now()

    def save_user(self, user_data):
        last_tweet_id = None
        last_tweet_time = None
        
        if 'status' in user_data.keys():
            last_tweet_id = user_data['status']['id_str']
            last_tweet_time = parser.parse(user_data['status']['created_at'])
        
        self.cursor.execute(
            "INSERT INTO twitter_user (id, screen_name, tweets_count, protected, last_tweet_id, last_tweet_time, last_update) VALUES (%s, %s, %s, %s, %s, %s, %s)",
            (
                user_data['id'], user_data['screen_name'], user_data['statuses_count'], user_data['protected'],
                last_tweet_id, last_tweet_time, self.now
            )
        )

    def do(self, twitter):
        logging.info('Requesting Users.')
        
        while True:
            try:
                data = twitter.lookup_user(user_id=self.ids)
                for user_data in data:
                    self.save_user(user_data)
                self.con.commit()
                return
                
            except TwythonRateLimitError:
                time.sleep((15*60)+10)
            except TwythonError, e:
                logging.error(e.msg)
                time.sleep(1)
            except psycopg2.Error:
                logging.error("Duplicated user?")
                return
            except Exception, e:
                logging.exception(e)
                time.sleep(2)

class Worker(threading.Thread):
    def __init__(self, job_queue, twitter):
        logging.info('Creating Worker.')
        super(Worker, self).__init__()
        self.daemon = True
        self.job_queue = job_queue
        self.twitter = twitter

    def run(self):
        while True:
            job = self.job_queue.get()
            try:
                job.do(self.twitter)
            except Exception, e:
                logging.exception(e)
            finally:
                self.job_queue.task_done()

def load_more_workers(twitters):
    print "Loading more workers"
    for twitter in twitters:
        worker = Worker(job_queue=job_queue, twitter=twitter)
        worker.start()

if __name__ == '__main__':
    #twython stuff
    client_args = {
        'verify': False
    }
    users = [
        {'OAUTH_TOKEN':'58598337-3Ut3XyQipUvNP8elbiaPc72b2zgZLjOuTkAsQ0XIV','OAUTH_TOKEN_SECRET':'q6ChEOjAaxTqUYYfAaln5CHSw50iy4JWOuo9ehv16xk'},
        {'OAUTH_TOKEN':'1626498452-2sbH8IQzxQTrKQTRFvQlBs6fKOQa3lCZzYIwoBT','OAUTH_TOKEN_SECRET':'3wjjoISE93WPtlKj7U5HHAf13W8cWeDnHSlTc703HSw'},
        {'OAUTH_TOKEN':'1621675346-b6589IcnejpQs6eMPAyAbTzFkyjCdmL3C8rxWmC','OAUTH_TOKEN_SECRET':'Lp0HLxwT2vHPBucoliHiOo2Oa8MkCmYqVGpVvUknOQ'},
        {'OAUTH_TOKEN':'1626527455-rpjZlooP3yxAsrdoDVgT7Tc3zdz3AAEWRB50EPi','OAUTH_TOKEN_SECRET':'IUtsKHcjWmCtDvH2OHl4sAM9QO8qES7imIeFEX7WJ1c'},
        {'OAUTH_TOKEN':'1626520256-Nall1hhzDXPvylDIIXMgftL6xNskNVeRcI1RoZH','OAUTH_TOKEN_SECRET':'86Y0YWL19xbowTucfcmp0XOlEtVA4AZKUibx1KQiMY'},
        {'OAUTH_TOKEN':'1646396670-qAChtM3tOjoapkCXPIE22bvfECWFBOeoP8Y5FYi','OAUTH_TOKEN_SECRET':'UoYqJ46dWZlfDzey4X7KcMA3oW59H4b3DSTB45xqIM'},
        {'OAUTH_TOKEN':'1646469529-DddaRLJ2ByUtdIEiQTTYNX1U4dfyLWOhK3Zlw9H','OAUTH_TOKEN_SECRET':'VxotU2S7KZKqZQkaLcIPZcmzHlnf0rW3ZM7ghvYFQ'},
        {'OAUTH_TOKEN':'1647550861-c9GBfwHIRfQdM44izOJHIUB5gmc47U4AcSHLGu8','OAUTH_TOKEN_SECRET':'ninQXIB85bAIZlqPwYvCykEABRqF3ANXBfUP3ZvQ'},
        {'OAUTH_TOKEN':'199298390-sA1VnCRvgvcUSDLv1ScM0BbV9ndxtwjmFc7Rzvtq','OAUTH_TOKEN_SECRET':'s2MTF9scweFUwMymSl44mviMKWpKG0L2cKCaQs9WgkY'}
    ]
    job_queue = Queue(10)
    twitters = []

    twitters.append(Twython(APP_KEY, access_token=ACCESS_TOKEN, client_args=client_args))
    for conf in users:
        twitter = Twython(APP_KEY, APP_SECRET, conf['OAUTH_TOKEN'], conf['OAUTH_TOKEN_SECRET'], client_args=client_args)
        twitters.append(twitter)

    for twitter in twitters[0:4]:
        worker = Worker(job_queue=job_queue, twitter=twitter)
        worker.start()


    con = psycopg2.connect("host=192.168.25.33 dbname='tweets' user='tweets' password='zxc123'")
    cursor = con.cursor()

    cursor.execute("""
        select id from twitter_user where id not in (select distinct user_id from tweet)
    """
    )
    rows = cursor.fetchall()
    
    t = threading.Timer(60*5, load_more_workers, [twitters[4:10]])

    users = []
    for row in rows:
        #users.append(row[0])
        job = RequestTimeline(row[0], con)
        job_queue.put(job)
        time.sleep(0.2)

    '''
    cursor = 0
    while True:
        if cursor*100 > len(users):
            break
        slice = users[(cursor*100):min((cursor+1)*100, len(users))]
        job = RequestUsersJob(slice, con)
        time.sleep(0.2)
        job_queue.put(job)
        cursor += 1
    '''
    print "Put all jobs"
    con.commit()
    job_queue.join()

